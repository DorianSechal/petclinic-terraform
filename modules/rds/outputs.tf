output "vet_db_endpoint" {
  description = "Endpoint for the vet-db instance"
  value       = aws_db_instance.db[0].endpoint
}

output "customer_db_endpoint" {
  description = "Endpoint for the customer-db instance"
  value       = aws_db_instance.db[1].endpoint
}

output "visit_db_endpoint" {
  description = "Endpoint for the visit-db instance"
  value       = aws_db_instance.db[2].endpoint
}

output "vet_db_password" {
  description = "Password for the vet-db instance"
  value       = aws_db_instance.db[0].password
}

output "customer_db_password" {
  description = "Password for the customer-db instance"
  value       = aws_db_instance.db[1].password
}

output "visit_db_password" {
  description = "Password for the visit-db instance"
  value       = aws_db_instance.db[2].password
}