resource "aws_s3_bucket" "petclinic-velero-backup" {
  bucket = var.velero_bucket_name
  
  tags = {
    Name        = "petclinic-velero-backup"
  }
}