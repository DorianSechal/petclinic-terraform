# Create IAM role and policy for Velero
resource "aws_iam_policy" "velero" {
  name        = "${var.cluster_name}-VeleroAccessPolicy"
  path        = "/"
  description = "Policy for Velero"

  policy = file("modules/iam/policy/velero_policy.json")
}