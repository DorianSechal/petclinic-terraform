# create an internet gateway for our vpc
resource "aws_internet_gateway" "petclinic_internet_gateway" {
  vpc_id = aws_vpc.petclinic_vpc.id
  depends_on = [
    aws_vpc.petclinic_vpc,
  ]

  tags = {
    Name = "petclinic-internet-gateway"
  }
}

# create a route to the internet gateway
resource "aws_route" "route_igw" {
  count                  = 2
  route_table_id         = aws_route_table.public_rts[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.petclinic_internet_gateway.id
}