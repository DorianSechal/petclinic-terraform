# TERRAFORM PETCLINIC

## Description
This project is a terraform project to deploy a petclinic application on AWS EKS. The application is deployed with a gitlab CI/CD pipeline. The pipeline is configured to deploy the application on the EKS cluster.
The application is deployed on a namespace called petclinic.
The application is deployed with a mysql database on RDS.

## Prerequisites
- Terraform
- AWS CLI
- Kubectl
- AWS Account and configured with aws configure

HOW TO CONFIGURE AWS

To configure AWS, run the following command:

```bash
aws configure
```
And follow the instructions

## Quick start
Create a file called terraform.tfvars and add the following variables:

```bash
petclinic_user = "petclinic" # Username for the petclinic application
petclinic_mysql_pwd = "password" # Password for the petclinic application
cidr_vpc = "10.0.0.0/16" # CIDR Block for AWS VPC
availability_zones = ["eu-west-3a", "eu-west-3b"] # AWS Availability zones
cidr_public_subnets = ["10.0.0.0/24", "10.0.1.0/24"] # CIDR Blocks for public subnets
cidr_private_subnets = ["10.0.2.0/24", "10.0.3.0/24"] # CIDR Blocks for private subnets
aws_account_id = "id"
cluster_name = "eks-cluster-name"
```

Create a .env file in the terraform folder and add the following variables:

```bash
GITLAB_TOKEN=token # Token to use the gitlab API (https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
GITLAB_PROJECT_ID=1 # Project ID of the project on gitlab (visible in the URL)
GITLAB_CI_TOKEN=token # Token to run the pipeline
OVH_APP_KEY=token
OVH_APP_SECRET=token
OVH_CONSUMER_KEY=token
PETCLINIC_DOMAIN_NAME=petclinic # Domain name of the application
GRAFANA_DOMAIN_NAME=grafana.petclinic # Domain name of the grafana application
DOMAIN_NAME=domain.fr
```

And run the script in the terraform folder:

```bash
./scripts/deploy.sh
```

The script will create all the infrastructure and deploy the application on the EKS cluster. Then the script will set up the gitlab variables and run the pipeline.


## Getting Started
Create a file called terraform.tfvars and add the following variables:

```bash
petclinic_user = "petclinic" # Username for the petclinic application
petclinic_mysql_pwd = "password" # Password for the petclinic application
cidr_vpc = "10.0.0.0/16" # CIDR Block for AWS VPC
availability_zones = ["eu-west-3a", "eu-west-3b"] # AWS Availability zones
cidr_public_subnets = ["10.0.0.0/24", "10.0.1.0/24"] # CIDR Blocks for public subnets
cidr_private_subnets = ["10.0.2.0/24", "10.0.3.0/24"] # CIDR Blocks for private subnets
aws_account_id = "id"
cluster_name = "eks-cluster-name"
```

To build this terraform project, run the following command:

```bash
terraform init
terraform plan
terraform apply -auto-approve
```

Take care that the terraform apply command will take approx 15 minutes to create the RDS database and the EKS cluster. Sometimes the terraform apply command will fail because the EKS cluster is not ready. In this case, you have to run the terraform apply command again.

To destroy this terraform project, run the following command:

```bash
terraform destroy -auto-approve
```

To get the Kubernetes cluster config, run the following command:

```bash
aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)
```

To be sure to use the right kubernetes config, run the following command:

```bash
export KUBECONFIG=~/.kube/config:~/.kube/config-$(terraform output -raw cluster_name)
```

To up the application, you have to update the KUBE_CONFIG variable in the gitlab CI/CD pipeline and run the pipeline.
```bash
cat ~/.kube/config
```

To set up the cluster OIDC provider, run the following command:

```bash
cluster_name=$(terraform output -raw cluster_name)
oidc_id=$(aws eks describe-cluster --name $cluster_name --query "cluster.identity.oidc.issuer" --output text | cut -d '/' -f 5)
echo $cluster_name
echo $oidc_id
aws iam list-open-id-connect-providers | grep $oidc_id | cut -d "/" -f4
eksctl utils associate-iam-oidc-provider --region $(terraform output -raw region) --cluster $(terraform output -raw cluster_name) --approve
aws_lb_controller_iam_role_name=$cluster_name-alb-controller
aws_account_id=$(aws sts get-caller-identity --query Account --output text)
cat >aws-load-balancer-controller-service-account.yaml <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    app.kubernetes.io/component: controller
    app.kubernetes.io/name: aws-load-balancer-controller
  name: aws-load-balancer-controller
  namespace: kube-system
  annotations:
    eks.amazonaws.com/role-arn: arn:aws:iam::$aws_account_id:role/$aws_lb_controller_iam_role_name
EOF
kubectl apply -f aws-load-balancer-controller-service-account.yaml
rm aws-load-balancer-controller-service-account.yaml
helm repo add eks https://aws.github.io/eks-charts
helm repo update eks
kubectl apply -k "github.com/aws/eks-charts/stable/aws-load-balancer-controller/crds?ref=master"
helm install aws-load-balancer-controller eks/aws-load-balancer-controller -n kube-system --set clusterName=$cluster_name --set serviceAccount.create=false --set serviceAccount.name=aws-load-balancer-controller
```

Install helm on the EKS cluster with the following command:

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get
```

Warning : be sure to have the right version of eksctl. If you have the wrong version, you can download the right version with the following command:

First, you have to delete the kubectl command:

```bash
bash /usr/local/bin/k3s-uninstall.sh
```

and then you can download the right version of kubectl:
```bash
curl -O https://s3.us-west-2.amazonaws.com/amazon-eks/1.28.3/2023-11-14/bin/linux/amd64/kubectl
```

and then you can download the right version of eksctl:
```bash
# for ARM systems, set ARCH to: `arm64`, `armv6` or `armv7`
ARCH=amd64
PLATFORM=$(uname -s)_$ARCH

curl -sLO "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_$PLATFORM.tar.gz"

# (Optional) Verify checksum
curl -sL "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_checksums.txt" | grep $PLATFORM | sha256sum --check

tar -xzf eksctl_$PLATFORM.tar.gz -C /tmp && rm eksctl_$PLATFORM.tar.gz

sudo mv /tmp/eksctl /usr/local/bin
```

To setup https on the application, you have to go to the AWS console and select the ALB created by the application. Then, you have to go to the Listeners tab and add a new listener on the port 443. You have to select the certificate created by the application.
Don't forget to setup the security group of the ALB to allow the port 443.

Install Velero CLI to be able to perform backups on EKS Cluster:

```bash
wget https://github.com/vmware-tanzu/velero/releases/download/v1.12.2/velero-v1.12.2-linux-amd64.tar.gz
tar -xvf velero-v1.12.2-linux-amd64.tar.gz -C /tmp
sudo mv /tmp/velero-v1.12.2-linux-amd64/velero /usr/local/bin
```

If you want to perform backup manually, first make sure you're working with the right cluster context
```bash
kubectl config current-context
velero backup create test -o yaml
velero backup create petclinic-backup
velero backup describe petclinic-backup
```
