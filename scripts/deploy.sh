#!/bin/bash

if [ -f ".env" ]; then
  export $(cat .env | xargs)
fi

echo "Déploiement de l'infrastructure..."

MAX_RETRIES=2
COUNTER=0

# Fonction pour déployer avec Terraform
apply_terraform() {
  echo "Tentative de déploiement: $((COUNTER+1))"
  terraform apply -auto-approve
  return $?
}

while [ $COUNTER -lt $MAX_RETRIES ]; do
  apply_terraform
  if [ $? -eq 0 ]; then
    echo "Déploiement réussi !"
    break
  else
    echo "Échec du déploiement, tentative de nouveau..."
    COUNTER=$((COUNTER+1))
  fi
done

# Vérifier si le déploiement a finalement échoué après toutes les tentatives
if [ $COUNTER -eq $MAX_RETRIES ]; then
  echo "Échec du déploiement après $MAX_RETRIES tentatives."
  exit 1
fi

# Mettre à jour le kubeconfig
rm -rf ~/.kube/config
aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)
export KUBECONFIG=~/.kube/config:~/.kube/config-$(terraform output -raw cluster_name)

# Récupérer les informations de l'infrastructure
cluster_name=$(terraform output -raw cluster_name)
oidc_id=$(aws eks describe-cluster --name $cluster_name --query "cluster.identity.oidc.issuer" --output text | cut -d '/' -f 5)
echo $cluster_name
echo $oidc_id

# Déployer le load balancer controller
echo "Déploiement du load balancer controller..."
aws iam list-open-id-connect-providers | grep $oidc_id | cut -d "/" -f4
eksctl utils associate-iam-oidc-provider --region $(terraform output -raw region) --cluster $(terraform output -raw cluster_name) --approve
aws_lb_controller_iam_role_name=$cluster_name-alb-controller
aws_account_id=$(aws sts get-caller-identity --query Account --output text)
cat >aws-load-balancer-controller-service-account.yaml <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    app.kubernetes.io/component: controller
    app.kubernetes.io/name: aws-load-balancer-controller
  name: aws-load-balancer-controller
  namespace: kube-system
  annotations:
    eks.amazonaws.com/role-arn: arn:aws:iam::$aws_account_id:role/$aws_lb_controller_iam_role_name
EOF
kubectl apply -f aws-load-balancer-controller-service-account.yaml
rm aws-load-balancer-controller-service-account.yaml

# Déployer le load balancer controller
helm repo add eks https://aws.github.io/eks-charts
helm repo update eks
kubectl apply -k "github.com/aws/eks-charts/stable/aws-load-balancer-controller/crds?ref=master"
helm upgrade --install aws-load-balancer-controller eks/aws-load-balancer-controller -n kube-system --set clusterName=$cluster_name --set serviceAccount.create=false --set serviceAccount.name=aws-load-balancer-controller

echo "Déploiement terminé !"

# Mise à jour des variables CI/CD gitlab pour le déploiement
echo "Mise à jour des variables CI/CD gitlab pour le déploiement..."

DB_URL_CUSTOMERS=$(terraform output -raw customer_db_endpoint_main)
curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
     "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables/DB_URL_CUSTOMERS" --form "value=jdbc:mysql://$DB_URL_CUSTOMERS/service_instance_db"

DB_URL_VISITS=$(terraform output -raw customer_db_endpoint_main)
curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
     "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables/DB_URL_VISITS" --form "value=jdbc:mysql://$DB_URL_VISITS/service_instance_db"

DB_URL_VETS=$(terraform output -raw customer_db_endpoint_main)
curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
     "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables/DB_URL_VETS" --form "value=jdbc:mysql://$DB_URL_VETS/service_instance_db"

DB_PASSWORD_CUSTOMERS=$(terraform output -raw customer_db_password_main)
curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
     "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables/DB_PASSWORD_CUSTOMERS" --form "value=$DB_PASSWORD_CUSTOMERS"

DB_PASSWORD_VISITS=$(terraform output -raw visit_db_password_main)
curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
     "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables/DB_PASSWORD_VISITS" --form "value=$DB_PASSWORD_VISITS"

DB_PASSWORD_VETS=$(terraform output -raw vet_db_password_main)
curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
     "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables/DB_PASSWORD_VETS" --form "value=$DB_PASSWORD_VETS"

# Update du kubeconfig
KUBE_CONFIG_CONTENT=$(cat ~/.kube/config)
curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
     "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables/KUBE_CONFIG" --form "value=$KUBE_CONFIG_CONTENT"


# Trigger le pipeline de déploiement
echo "Trigger le pipeline de déploiement..."
curl -X POST \
     --fail \
     -F token=$GITLAB_CI_TOKEN \
     -F ref=master \
     https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/trigger/pipeline


# Sleep pour laisser le temps au pipeline de se terminer
echo ""
echo "Sleep 5 minutes pour laisser le temps au pipeline de se terminer..."
sleep 300

  # Paramètres de retry du load balancer
  maxAttempts=10
  delay=30

  # Récupérer l'URL du Load Balancer pour API Gateway
  attempt=1
  while [ $attempt -le $maxAttempts ]; do
      echo "Tentative $attempt sur $maxAttempts pour obtenir l'URL du Load Balancer pour API Gateway..."
      apiGatewayLoadBalancerURL=$(kubectl get ingress api-gateway -n spring-petclinic -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')

      if [ -n "$apiGatewayLoadBalancerURL" ]; then
          echo "URL du Load Balancer pour API Gateway trouvée: $apiGatewayLoadBalancerURL"
          break
      else
          echo "URL du Load Balancer pour API Gateway non trouvée, nouvelle tentative dans $delay secondes..."
          sleep $delay
      fi

      ((attempt++))
  done

  # Vérifier si l'URL est récupérée
  if [ -z "$apiGatewayLoadBalancerURL" ]; then
      echo "Échec après $maxAttempts tentatives pour API Gateway."
      exit 1
  fi

  # Répéter le processus pour Grafana
  attempt=1
  while [ $attempt -le $maxAttempts ]; do
      echo "Tentative $attempt sur $maxAttempts pour obtenir l'URL du Load Balancer pour Grafana..."
      grafanaLoadBalancerURL=$(kubectl get ingress grafana-service -n spring-petclinic -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')

      if [ -n "$grafanaLoadBalancerURL" ]; then
          echo "URL du Load Balancer pour Grafana trouvée: $grafanaLoadBalancerURL"
          break
      else
          echo "URL du Load Balancer pour Grafana non trouvée, nouvelle tentative dans $delay secondes..."
          sleep $delay
      fi

      ((attempt++))
  done

  # Vérifier si l'URL est récupérée
  if [ -z "$grafanaLoadBalancerURL" ]; then
      echo "Échec après $maxAttempts tentatives pour Grafana."
      exit 1
  fi
  
# Paramètres de l'API OVH
ttl=60 # Temps de vie de l'enregistrement
method="POST"
apiUrl="https://api.ovh.com/1.0/domain/zone/$DOMAIN_NAME/record"

# Corps de la requête pour Petclinic
petclinicBody="{\"fieldType\":\"CNAME\", \"subDomain\":\"$PETCLINIC_DOMAIN_NAME\", \"target\":\"$apiGatewayLoadBalancerURL.\", \"ttl\":$ttl}"

# Corps de la requête pour Grafana
grafanaBody="{\"fieldType\":\"CNAME\", \"subDomain\":\"$GRAFANA_DOMAIN_NAME\", \"target\":\"$grafanaLoadBalancerURL.\", \"ttl\":$ttl}"

# Fonction pour envoyer une requête à l'API OVH
envoyerRequete() {
    local body=$1

    # Générer la signature
    timestamp=$(curl -s https://api.ovh.com/1.0/auth/time)
    toSign="$OVH_APP_SECRET+$OVH_CONSUMER_KEY+$method+$apiUrl+$body+$timestamp"
    signature=$(/bin/echo -n "$toSign" | sha1sum | cut -d" " -f1)

    # Envoyer la requête
    curl -X $method -H "Content-Type: application/json" \
        -H "X-Ovh-Application: $OVH_APP_KEY" \
        -H "X-Ovh-Timestamp: $timestamp" \
        -H "X-Ovh-Consumer: $OVH_CONSUMER_KEY" \
        -H "X-Ovh-Signature: \$1\$${signature}" \
        -d "$body" \
        "$apiUrl"
}

# Appeler la fonction pour Petclinic et Grafana
envoyerRequete "$petclinicBody"
envoyerRequete "$grafanaBody"

# On rafraîchit les enregistrements DNS
method="POST"
rafraichirZone() {

    apiUrl="https://api.ovh.com/1.0/domain/zone/$DOMAIN_NAME/refresh"

    # Générer la signature
    timestamp=$(curl -s https://api.ovh.com/1.0/auth/time)
    toSign="$OVH_APP_SECRET+$OVH_CONSUMER_KEY+$method+$apiUrl+$body+$timestamp"
    signature=$(/bin/echo -n "$toSign" | sha1sum | cut -d" " -f1)

    # Envoyer la requête
    curl -X $method -H "Content-Type: application/json" \
        -H "X-Ovh-Application: $OVH_APP_KEY" \
        -H "X-Ovh-Timestamp: $timestamp" \
        -H "X-Ovh-Consumer: $OVH_CONSUMER_KEY" \
        -H "X-Ovh-Signature: \$1\$${signature}" \
        "$apiUrl"
}

rafraichirZone

# On boucle pour attendre que les enregistrements DNS soient bien propagés
echo "Attente de la propagation des enregistrements DNS..."

# Fonction pour vérifier si l'enregistrement DNS est propagé
verifierPropagation() {
    local subDomain=$1
    local target=$2
    local maxAttempts=20
    local attempt=1
    local delay=60

    while [ $attempt -le $maxAttempts ]; do
        echo "Tentative $attempt sur $maxAttempts pour vérifier la propagation de $subDomain.$DOMAIN_NAME..."

        # Récupérer l'adresse IP de l'enregistrement DNS
        dnsIP=$(dig +short $subDomain.$DOMAIN_NAME)

        if [ "$dnsIP" = "$target." ]; then
            echo "L'enregistrement DNS pour $subDomain.$DOMAIN_NAME est propagé."
            return 0
        else
            echo "L'enregistrement DNS pour $subDomain.$DOMAIN_NAME n'est pas encore propagé, nouvelle tentative dans $delay secondes..."
            sleep $delay
        fi

        attempt=$((attempt + 1))
    done

    echo "Échec de la propagation de $subDomain.$DOMAIN_NAME après $maxAttempts tentatives."
    return 1
}

# Appeler la fonction pour Petclinic et Grafana
verifierPropagation $PETCLINIC_DOMAIN_NAME $apiGatewayLoadBalancerURL
verifierPropagation $GRAFANA_DOMAIN_NAME $grafanaLoadBalancerURL

echo "Déploiement terminé !"